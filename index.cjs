let toggleId = document.getElementById("toggle");
let basicPriceId = document.getElementById("basicId");
let professionalPriceId = document.getElementById("premiumId");
let masterPriceId = document.getElementById("masterId");

toggleId.addEventListener("click", () => {
  if (toggleId.checked) {
    basicPriceId.textContent = "$19.99";
    professionalPriceId.textContent = "$24.99";
    masterPriceId.textContent = "$39.99";
  } else {
    basicPriceId.textContent = "$199.99";
    professionalPriceId.textContent = "$249.99";
    masterPriceId.textContent = "$399.99";
  }
});
